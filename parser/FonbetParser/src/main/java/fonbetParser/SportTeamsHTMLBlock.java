package fonbetParser;

import java.util.List;

public class SportTeamsHTMLBlock {

    public SportTeamsHTMLBlock(String sportNameLeague, List<String> teamsName){
        this.sportNameLeague = sportNameLeague;
        this.teamsNames = teamsName;
    }

    private String sportNameLeague;

    private List<String> teamsNames;

    //get,set

    public String getSportNameLeague(){
        return this.sportNameLeague;
    }

    public void setSportNameLeague(String sportNameLeague){
        this.sportNameLeague = sportNameLeague;
    }

    public List<String> getTeamsNames(){
        return this.teamsNames;
    }

    public void setTeamsNames(List<String> teams){
        this.teamsNames = teams;
    }

}
