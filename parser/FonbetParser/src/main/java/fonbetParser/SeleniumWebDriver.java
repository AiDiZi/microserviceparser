package fonbetParser;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class SeleniumWebDriver {

    private WebDriver _webDriver;

    public void CloseWindow(){
        _webDriver.close();
    }

    public SeleniumWebDriver(WebDriver webDriver, String uri){
        _webDriver = webDriver;
        _webDriver.get(uri);
    }

    public <O> List<O> GetElementsWithCommonParentClassName(String parentClassName,
                                                            ConvertInterface<O> convertFunc){
        var parentsElement = _webDriver.findElements(By.className(parentClassName));

        return parentsElement.stream().map(item -> {
            return convertFunc.convert(item);
        }).toList();
    }



}
