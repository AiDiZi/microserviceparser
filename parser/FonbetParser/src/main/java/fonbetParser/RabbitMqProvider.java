package fonbetParser;

import RabbitMQConstants.FonbetContants;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.nio.charset.StandardCharsets;

public class RabbitMqProvider {

    public static void SendData(String data){
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("rabbitmq");
        try(Connection connection = connectionFactory.newConnection()){
            Channel channel = connection.createChannel();
            //channel.exchangeDeclare(FonbetContants.ExchangerName, "direct");
            channel.queueDeclare(FonbetContants.QueueName, false, false, false, null);
            //channel.queueBind(FonbetContants.QueueName, FonbetContants.ExchangerName, FonbetContants.FonbetRoutingKey, null);
            channel.basicPublish(FonbetContants.ExchangerName,FonbetContants.FonbetRoutingKey,null, data.getBytes(StandardCharsets.UTF_8));
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
