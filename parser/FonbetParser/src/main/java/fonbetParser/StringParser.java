package fonbetParser;

import java.util.*;
import java.util.regex.Pattern;

public class StringParser {

    public List<String> GetDataFromStr(String str,
                                       String splitter,
                                       HashMap<Integer, IStringParser> conditionFuncMap) throws Exception{
        var arr = Arrays.stream(str.split(splitter))
                .map(item -> item.trim()).toList();
        var nowCondFunc = conditionFuncMap.get(arr.size());
        if(nowCondFunc != null){
            return nowCondFunc.parse(arr);
        }
        else
            throw new Exception("unknow str pattern");
    }

    public List<String> GetDataFromStr(String str , String splitter, int partCount) {
        var arr = Arrays.stream(str.split(Pattern.quote(splitter)))
                .map(item -> item.trim()).toList();
        var data = new ArrayList<String>();
        if(arr.size() >= partCount) {
            this.FillArrIndexes(arr, partCount, data);
        }
        return data;
    }

    private String ContactAllItems(List<String> arr){
        StringBuilder outStr = new StringBuilder("");
        for(String str : arr)
            outStr.append(str).append(" ");
        return outStr.toString();
    }

    private void FillArrIndexes(List<String> arr ,int countPart, List<String> data){
        for(int i = 0; i < countPart; i++){
            if(i == countPart - 1){
                var oldArr = new ArrayList<String>();
                for(int j = i; j < arr.size(); j++)
                    oldArr.add(arr.get(j));
                data.add(ContactAllItems(oldArr));
            }
            else {
                data.add(arr.get(i));
            }
        }
    }
}
