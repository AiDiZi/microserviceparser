package fonbetParser;

import org.openqa.selenium.WebElement;

@FunctionalInterface
public interface ConvertInterface<O> {
    O convert(WebElement element);
}
