package fonbetParser;

import java.util.List;

@FunctionalInterface
public interface IStringParser {
    List<String> parse(List<String> arr);
}
