package fonbetParser;

import Models.MatchModel;
import Models.Team;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.By;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;


public class Application {

    private static final int _timeoutMilliSecond = 500;

    private  static  final  ObjectMapper _objectMapper = new ObjectMapper();

    private static final StringParser _stringParser = new StringParser();

    private final static RabbitMqProvider _rabbitMqProvider = new RabbitMqProvider();

    private final static String _uri = "https://www.fonbet.ru/live/";

    private final static String _sportLeagueClassName = "table__title-text";

    private final static String _commandsClassName = "table__match-title-text";

    private static volatile MatchModel[] _models;

    private static SeleniumWebDriver webParser;

    public static void main(String[] args){
        var driver = GetDriver();
        try {
            webParser = new SeleniumWebDriver(driver, _uri);
            var dataForParse = GetDataForParse(webParser);
            FillModelsForDB(dataForParse);
            if(_models.length != 0) {
                var jsonModels = _objectMapper.writeValueAsString(_models);
                _rabbitMqProvider.SendData(jsonModels);
            }
            Thread.sleep(_timeoutMilliSecond);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        finally {
            webParser.CloseWindow();
        }
        main(args);
    }

    private static WebDriver GetDriver(){
        System.setProperty("webdriver.gecko.driver","/usr/bin/geckodriver");
        var options = new FirefoxOptions();
        options.addArguments("--headless", "--disable-gpu","--no-sandbox", "--start-maximized");
        options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        var driver = new FirefoxDriver(options);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        return driver;
    }

    private static List<SportTeamsHTMLBlock> GetDataForParse(SeleniumWebDriver webParser){
        var modelsForParse = webParser.GetElementsWithCommonParentClassName("table__body",
                (WebElement element) -> {
                    var sportNameLeague = element.findElement(By.className(_sportLeagueClassName));
                    var teamsNames = element.findElements(By.className(_commandsClassName)).stream()
                            .filter(item -> {
                                var className = item.getAttribute("class");
                                return className.equals(_commandsClassName);
                            })
                            .map(item ->
                                    item.getText())
                            .distinct()
                            .toList();
                    return new SportTeamsHTMLBlock(sportNameLeague.getText(), teamsNames);
                });
        return modelsForParse;
    }

    private static void FillModelsForDB(List<SportTeamsHTMLBlock> modelsForParse) throws Exception{
        var matchModelsFutures = new ArrayList<CompletableFuture<Void>>();
        _models = new MatchModel[modelsForParse.size()];
        for(int i = 0; i < _models.length; i++){
            matchModelsFutures.add(GetMatchModel(modelsForParse.get(i), i));
        }
        var allTasks = CompletableFuture.allOf(matchModelsFutures.toArray(new CompletableFuture[0]));
        allTasks.get();
    }

    private static CompletableFuture<Void> GetMatchModel(SportTeamsHTMLBlock sportTeamsHTMLBlock, int index){
        return CompletableFuture.runAsync(() -> {
            var conditionFunc = new HashMap<Integer, IStringParser>();
            conditionFunc.put(2, (List<String> arr) -> {
                return arr;
            });
            conditionFunc.put(3, (List<String> arr) -> {
                return List.of(arr.get(0), arr.get(2));
            });
            conditionFunc.put(4, (List<String> arr) -> {
                return List.of(arr.get(0), arr.get(2));
            });
            conditionFunc.put(5, (List<String> arr) -> {
                return List.of(arr.get(0), arr.get(4));
            });
            conditionFunc.put(6, (List<String> arr) -> {
                return List.of(arr.get(0), arr.get(2));
            });
            conditionFunc.put(7, (List<String> arr) -> {
                return List.of(arr.get(0), arr.get(2));
            });
            conditionFunc.put(8, (List<String> arr) -> {
                return List.of(arr.get(0), arr.get(2));
            });
            try {
                var sportLeague = _stringParser.GetDataFromStr(sportTeamsHTMLBlock.getSportNameLeague(),
                        "\\.", conditionFunc);
                var sport = (sportLeague.size() >= 1) ? sportLeague.get(0) : "";
                var league = (sportLeague.size() == 2) ? sportLeague.get(1) : "";
                var teams = sportTeamsHTMLBlock.getTeamsNames().stream().map(item -> {
                    var teamsList = _stringParser.GetDataFromStr(item, "\u2014", 2);
                    var firstTeam = (teamsList.size() >= 1) ? teamsList.get(0) : "";
                    var secondTeam = (teamsList.size() == 2) ? teamsList.get(1) : "";
                    return new Team(firstTeam, secondTeam);
                }).toList();

                var model = new MatchModel(sport, league, teams);
                _models[index] = model;
            }
            catch (Exception e){
                _models[index] = new MatchModel("", "", new ArrayList<Team>());
            }
        });
    }
}
