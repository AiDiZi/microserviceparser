package com.example.backendfonbet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendFonbetApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackendFonbetApplication.class, args);
    }
}
