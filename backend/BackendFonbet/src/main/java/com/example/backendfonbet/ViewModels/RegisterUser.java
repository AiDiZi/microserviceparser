package com.example.backendfonbet.ViewModels;

import com.example.backendfonbet.Models.Role;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotBlank;

public class RegisterUser extends BaseUser{

    public RegisterUser(){}

    public RegisterUser(String userName, String password, Role role){
        super(userName, password);
        this.role = role;
    }

    private Role role;

    //get, set
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}

