package com.example.backendfonbet.Controllers;

import com.example.backendfonbet.JWT.JWTProvider;
import com.example.backendfonbet.Models.User;
import com.example.backendfonbet.Repositories.RoleRepository;
import com.example.backendfonbet.Services.UserService;
import com.example.backendfonbet.ViewModels.RegisterUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import com.example.backendfonbet.ViewModels.BaseUser;
import com.example.backendfonbet.ViewModels.Token;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/User")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService _userService;

    @Autowired
    private RoleRepository _roleRepository;

    @Autowired
    private JWTProvider _jwtProvider;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }


    @PostMapping(value = "/Register")
    public ResponseEntity Register(@Valid @RequestBody RegisterUser registerUser){
        var nowUserOptional = _userService.findByLogin(registerUser.getUserName());
        if(!nowUserOptional.isPresent()) {
            var roleTable = _roleRepository.findByRole(registerUser.getRole());
            if(roleTable != null) {
                var uuid = UUID.randomUUID();
                var user = new User();
                user.setUserName(registerUser.getUserName());
                user.setPassword(registerUser.getPassword());
                user.setId(uuid);
                var role = _roleRepository.findByRole(registerUser.getRole());
                user.setRole(role);
                _userService.saveUser(user);
                var token = _jwtProvider.generateToken(user.getUserName());
                return ResponseEntity.ok()
                        .body(new Token(token));
            }
            else {
                return ResponseEntity.badRequest()
                        .body("such role is not exist");
            }
        }
        else {
            return ResponseEntity.badRequest()
                    .body(new RegisterUser("user with such username already exist", "", registerUser.getRole()));
        }
    }


    @PostMapping(value = "/Auth")
    public ResponseEntity Auth(@Valid @RequestBody BaseUser authUser){
        try {
            var user = _userService.findByLoginAndPassword(authUser.getUserName(), authUser.getPassword());
            String token = _jwtProvider.generateToken(user.getUserName());
            return ResponseEntity.ok()
                    .body(new Token(token));
        }
        catch (Exception e){
            return ResponseEntity.badRequest()
                    .body(new BaseUser("user with such password or username is not exist", ""));
        }
    }

}
