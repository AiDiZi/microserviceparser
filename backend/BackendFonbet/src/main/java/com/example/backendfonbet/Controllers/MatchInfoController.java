package com.example.backendfonbet.Controllers;

import com.example.backendfonbet.Models.MatchInfo;
import com.example.backendfonbet.Repositories.MatchInfoRepository;
import io.swagger.v3.oas.annotations.headers.Header;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;


@RestController
@RequestMapping("/MatchInfo")
@CrossOrigin
public class MatchInfoController {

    @Autowired
    private MatchInfoRepository _matchInfoRepository;

    @GetMapping("/GetAll")
    public ResponseEntity GetAllMatchsInfo(){
        var infos = _matchInfoRepository.findAll();
        return ResponseEntity.ok().body(infos);
    }

    @GetMapping("/GetLimit/{count}")
    public ResponseEntity GetLimitMatchInfo(@PathVariable int count){
        var matchInfo = _matchInfoRepository.GetPartMatchInfo(count);
        return ResponseEntity.ok().body(matchInfo);
    }
}
