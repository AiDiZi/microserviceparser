package com.example.backendfonbet.Configs;

import Models.MatchModel;
import RabbitMQConstants.FonbetContants;
import com.example.backendfonbet.Models.MatchInfo;
import com.example.backendfonbet.Models.PlayInfo;
import com.example.backendfonbet.Repositories.MatchInfoRepository;
import com.example.backendfonbet.Repositories.PlayInfoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class RabbitMqConfig {

    @Autowired
    private ObjectMapper _objectMapper;

    @Autowired
    private MatchInfoRepository _matchInfoRepository;

    @Autowired
    private PlayInfoRepository _playInfoRepository;


    @Bean
    public Queue queue(){
        return new Queue(FonbetContants.QueueName, false);
    }

    @Bean
    public TopicExchange exchange(){
        return new TopicExchange(FonbetContants.ExchangerName);
    }

    @Bean
    public Binding binding(Queue queue, TopicExchange exchanger){
        return BindingBuilder.bind(queue).to(exchanger).with("fonbetRoutingKey");
    }

    @Transactional
    public void HandleMessage(Message message){
        try {
            var collectionType = _objectMapper.getTypeFactory()
                    .constructCollectionType(ArrayList.class, MatchModel.class);
            List<MatchModel> matchModel = _objectMapper.readValue(message.getBody(), collectionType);
            _matchInfoRepository.deleteAll();
            matchModel.stream().forEach(item -> {
                var matchInfo = MatchInfo.GetMatchInfoFromMatchModel(item);
                matchInfo.SetPlayInfoFromTeams(item.getTeams());
                _matchInfoRepository.save(matchInfo);
            });
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Bean
    public SimpleMessageListenerContainer container(ConnectionFactory connectionFactory){
        SimpleMessageListenerContainer messageListenerContainer = new SimpleMessageListenerContainer();
        messageListenerContainer.setConnectionFactory(connectionFactory);
        messageListenerContainer.setQueueNames(FonbetContants.QueueName);
        messageListenerContainer.setupMessageListener((message) -> HandleMessage(message));
        return messageListenerContainer;
    }

}
