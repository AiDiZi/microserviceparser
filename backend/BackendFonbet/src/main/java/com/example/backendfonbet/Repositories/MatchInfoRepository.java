package com.example.backendfonbet.Repositories;

import com.example.backendfonbet.Models.MatchInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MatchInfoRepository extends CrudRepository<MatchInfo, UUID> {

    @Query(value = "select * from match_info limit :count", nativeQuery = true)
    List<MatchInfo> GetPartMatchInfo(@Param("count") int count);

}
