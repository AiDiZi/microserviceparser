package com.example.backendfonbet.ViewModels;

import javax.validation.constraints.NotBlank;

public class BaseUser {

    public BaseUser(){}

    public BaseUser(String userName, String password){
        this.userName = userName;
        this.password = password;
    }

    @NotBlank(message = "first name can't be null or whitespace")
    private String userName;


    @NotBlank(message = "password can't be null or whitespace")
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
