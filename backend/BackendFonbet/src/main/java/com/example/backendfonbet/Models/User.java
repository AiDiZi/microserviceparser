package com.example.backendfonbet.Models;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "person")
public class User {

    public User(){}

    @Id
    private UUID id;

    @Column(nullable = false, unique = true)
    private String userName;

    @Column(nullable = false)
    private String password;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "roleId")
    private RoleTable role;

    //get ,set

    public UUID getId() {
        return id;
    }

    public void setId(UUID id){
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleTable getRole() {
        return role;
    }

    public void setRole(RoleTable role) {
        this.role = role;
    }

    public RoleTable get() {
        return this.role;
    }
}
