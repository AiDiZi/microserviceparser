package com.example.backendfonbet.Models;

import Models.Team;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "playInfo")
public class PlayInfo {

    public static PlayInfo GetPlayInfoFromTeam(Team team, MatchInfo matchInfo){
        return new PlayInfo(UUID.randomUUID(), team.getFirtsTeam(), team.getSecondTeam(), matchInfo);
    }

    public PlayInfo(UUID id, String firstTeam, String secondTeam, MatchInfo matchInfo){
        this.id = id;
        this.firtsTeam = firstTeam;
        this.secondTeam = secondTeam;
        this.matchInfo = matchInfo;
    }

    public PlayInfo(){}

    @Id
    private UUID id;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "matchIdInfo")
    private MatchInfo matchInfo;

    private String firtsTeam;

    private String secondTeam;

    //get set

    public UUID getId() {
        return id;
    }

    public String getFirtsTeam() {
        return firtsTeam;
    }

    public void setFirtsTeam(String firtsTeam) {
        this.firtsTeam = firtsTeam;
    }

    public String getSecondTeam() {
        return secondTeam;
    }

    public void setSecondTeam(String secondTeam) {
        this.secondTeam = secondTeam;
    }

    public void setMatchInfo(MatchInfo matchInfo){
        this.matchInfo = matchInfo;
    }
}
