package com.example.backendfonbet.Repositories;

import com.example.backendfonbet.Models.Role;
import com.example.backendfonbet.Models.RoleTable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepository extends CrudRepository<RoleTable, UUID> {

    RoleTable findByRole(Role role);


}
