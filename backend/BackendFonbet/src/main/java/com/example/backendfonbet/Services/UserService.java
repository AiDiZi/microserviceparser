package com.example.backendfonbet.Services;

import com.example.backendfonbet.Models.RoleTable;
import com.example.backendfonbet.Models.User;
import com.example.backendfonbet.Repositories.RoleRepository;
import com.example.backendfonbet.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class UserService {

    @Autowired
    private UserRepository _userRepository;

    @Autowired
    private RoleRepository _roleRepository;

    public User saveUser(User userEntity) {
        RoleTable userRole = _roleRepository.findByRole(userEntity.getRole().getRole());
        userEntity.setRole(userRole);
        userEntity.setPassword(userEntity.getPassword());
        return _userRepository.save(userEntity);
    }

    public Optional<User> getUserById(UUID id){
        return _userRepository.findById(id);
    }

    public Optional<User> findByLogin(String userName) {
        return _userRepository.findByUserName(userName);
    }

    public User findByLoginAndPassword(String login, String password) {
        var userEntity = findByLogin(login).get();
        if (userEntity != null) {
            if (password.equals(userEntity.getPassword())) {
                return userEntity;
            }
        }
        return null;
    }

}
