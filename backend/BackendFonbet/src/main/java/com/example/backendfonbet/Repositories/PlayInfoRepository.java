package com.example.backendfonbet.Repositories;

import com.example.backendfonbet.Models.PlayInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PlayInfoRepository extends CrudRepository<PlayInfo, UUID> {
}
