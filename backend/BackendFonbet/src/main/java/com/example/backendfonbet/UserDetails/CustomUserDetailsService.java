package com.example.backendfonbet.UserDetails;

import com.example.backendfonbet.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component("customerUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService _userService;

    @Override
    public CustomUserDetails loadUserByUsername(String userName) throws UsernameNotFoundException{
        var user = _userService.findByLogin(userName).get();
        return CustomUserDetails.fromUserEntityToCustomUserDetails(user);
    }

}
