package com.example.backendfonbet.Models;

import Models.MatchModel;
import Models.Team;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "matchInfo")
public class MatchInfo {

    public static MatchInfo GetMatchInfoFromMatchModel(MatchModel matchModel){
        var matchInfo = new MatchInfo(UUID.randomUUID(),matchModel.getSportName(), matchModel.getLeague());
        return matchInfo;
    }

    public void SetPlayInfoFromTeams(List<Team> teamsInfo){
        var playsInfo = teamsInfo.stream().map(item -> {
            var playInfo = PlayInfo.GetPlayInfoFromTeam(item, this);
            return playInfo;
        }).toList();
        this.setPlaysInfo(playsInfo);
    }

    public MatchInfo(UUID id, String sportName, String league){
        this.id = id;
        this.sportName = sportName;
        this.league = league;
    }

    public MatchInfo(){}

    @Id
    private UUID id;

    @Column(name = "sport")
    private String sportName;

    private String league;

    @OneToMany(mappedBy = "matchInfo", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<PlayInfo> playInfo;

    //get,sets
    public UUID getId() {
        return id;
    }

    public void setId(UUID id){
        this.id = id;
    }

    public String getSportName(){
        return sportName;
    }

    public void setSportName(String sportName) {
        this.sportName = sportName;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public List<PlayInfo> getPlayInfo() {
        return playInfo;
    }

    public void setPlaysInfo(List<PlayInfo> playInfo) {
        this.playInfo = playInfo;
    }
}
