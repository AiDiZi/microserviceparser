package com.example.backendfonbet.Models;

public enum Role {
    ROLE_ADMIN,
    ROLE_USER;
}
