SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: match_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.match_info (
    id uuid NOT NULL,
    league character varying(255),
    sport character varying(255)
);


ALTER TABLE public.match_info OWNER TO postgres;



CREATE TABLE public.person (
    id uuid NOT NULL,
    password character varying(255),
    user_name character varying(255),
    role_id uuid NOT NULL
);


ALTER TABLE public.person OWNER TO postgres;


CREATE TABLE public.play_info (
    id uuid NOT NULL,
    firts_team character varying(255),
    second_team character varying(255),
    match_id_info uuid NOT NULL
);


ALTER TABLE public.play_info OWNER TO postgres;


CREATE TABLE public.role (
    id uuid NOT NULL,
    role integer
);


ALTER TABLE public.role OWNER TO postgres;


COPY public.match_info (id, league, sport) FROM stdin;
\.





COPY public.play_info (id, firts_team, second_team, match_id_info) FROM stdin;
\.



COPY public.role (id, role) FROM stdin;
3930c333-8e2c-4f3a-a5c8-22e610154f52	0
76a92947-6d7a-4217-ae51-c28809719203	1
\.


--
-- Name: match_info match_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.match_info
    ADD CONSTRAINT match_info_pkey PRIMARY KEY (id);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- Name: play_info play_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.play_info
    ADD CONSTRAINT play_info_pkey PRIMARY KEY (id);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: person fkfqfeq5nokuewxxtb44t9lw012; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT fkfqfeq5nokuewxxtb44t9lw012 FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- Name: play_info fkk03g7c7brhsgcji6edticutky; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.play_info
    ADD CONSTRAINT fkk03g7c7brhsgcji6edticutky FOREIGN KEY (match_id_info) REFERENCES public.match_info(id);


--
-- PostgreSQL database dump complete
--

